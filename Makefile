IMAGE_NAME=$(shell basename $(CURDIR))

.PHONY: all build test push

all: build test push

build:
	docker build -t regreb/$(IMAGE_NAME) .

test:
	@if [ -d "spec" ]; then bundle && bundle exec rspec; fi

push:
	docker push regreb/$(IMAGE_NAME)
