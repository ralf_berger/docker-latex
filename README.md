# Docker image for LaTeX typesetting

This image contains a texlive-full installation on bitnami/minideb:latest.

* [Docker Hub](https://hub.docker.com/r/regreb/latex/)
